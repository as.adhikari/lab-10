package prob3;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static <T extends Number> double sum(List<T> list){
        double sum = 0;
        for(T element:list){
            sum += element.doubleValue();
        }

        return sum;
    }

    public static void main(String[] args){
        List<Integer> intList = Arrays.asList(1,5,7,33,20,44,11);
        List<Double> dblList = Arrays.asList(45.0,33.3,21.2,99.0);

        System.out.println(sum(intList));
        System.out.println(sum(dblList));
    }
}
