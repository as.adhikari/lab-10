package prob2;

import java.util.*;

public class Main {
    public static <T extends Comparable<? super T>> T secondSmallest(List<T> list){

        Optional<T> secondSmallest = list.stream()
                .skip(1)
                .sorted()
                .findFirst();

        return secondSmallest.orElse(null);
        
//        if(list.size() < 1) return null;
//        Collections.sort(list);
//
//        return list.get(1);
    }

    public static void main(String[] args){
        List<Integer> intList = Arrays.asList(1,3,5,6,7,90,3,4,2,55);
        System.out.println(secondSmallest(intList));

        List<String> strList = Arrays.asList("Ashok","Adhikari","Java","Programming","Modern","Programming");
        System.out.println(secondSmallest(strList));

        List<Double> dblList = new ArrayList<>();
        System.out.println(secondSmallest(dblList));
    }
}
